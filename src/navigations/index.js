import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { fromLeft, fromRight, fromBottom, zoomOut, zoomIn, fadeIn, fadeOut, fromTop, flipY } from 'react-navigation-transitions';

//views
import Splash from '../screens/splash';
import SocialLogin from '../screens/socialLogin';
import Registration from '../screens/registration';
import Confirmaemail from '../screens/registration/confiremail';
import List from '../screens/List';
import Login from '../screens/login';
import Recovery from "../screens/recoverypassword";
import Productos from "../screens/List productos"
import DetallesProd from "../screens/detalle producto"
import Menu from "../screens/menu"
import Direccio from "../screens/screenmenu/direccion"
import Pago from "../screens/screenmenu/pago"
import Fromcreditcard from "../screens/screenmenu/formulariostripe"
import Frompaypal from "../screens/screenmenu/formulariopaypal"
import Myads from "../screens/screenmenu/myads"
import Review from "../screens/screenmenu/review"
import Account from "../screens/screenmenu/acount-detail"
import Listadedeseos from "../screens/screenmenu/wishlist"
import Ordenes from "../screens/screenmenu/orders"
import OrdenesDitails from "../screens/screenmenu/detalledelaorden"
import Pedido from "../screens/screenmenu/pedidos"
import PedidoDitails from "../screens/screenmenu/detalledelpedido"
import Payment from "./../screens/payment"
import Cupon from "../screens/screenmenu/cupon"
import Exito from "../screens/screenmenu/compraexitosa"
import Error from "../screens/screenmenu/compraerror"
import ReviewClient from "./../screens/review"
import Reportproblen from "./../screens/reportproblem"
import PublicProfile from "./../screens/publicProfile"
import Filtros from "./../screens/filtros"
import Notification from "./../screens/notification"
import Condicionesdeuso from './../screens/privacidad/condicionesdeuso'
import Regalo from "../screens/screenmenu/regalo"
import Agenda from './../screens/screenmenu/agenda';
import Categorias from '../screens/filtros/categorias';
import Mapa from '../screens/filtros/mapa';

const handleCustomTransition = ({ scenes }) => {
    const prevScene = scenes[scenes.length - 2];
    const nextScene = scenes[scenes.length - 1];
   
    // Custom transitions go there
    if (prevScene
      && prevScene.route.routeName === 'List'
      && nextScene.route.routeName === 'Menu') {
      return fromBottom(500);

    } else if (prevScene
      && prevScene.route.routeName === 'Productos'
      && nextScene.route.routeName === 'DetallesProd') {
      return zoomIn(500);
    }

   else if (prevScene
    && prevScene.route.routeName === 'List'
    && nextScene.route.routeName === 'DetallesProd') {
    return zoomIn(500);
  }

    if (prevScene
        && prevScene.route.routeName === 'DetallesProd'
        && nextScene.route.routeName === 'PublicProfile') {
        return fromBottom(500);
      }

      if (prevScene
        && prevScene.route.routeName === 'SocialLogin'
        && nextScene.route.routeName === 'List') {
        return zoomIn(500);
      }
      if (prevScene
        && prevScene.route.routeName === 'List'
        && nextScene.route.routeName === 'Productos') {
        return fromBottom(500);
      }

      if (prevScene
        && prevScene.route.routeName === 'DetallesProd'
        && nextScene.route.routeName === 'Agenda') {
        return fadeIn(500);
      }


      if (prevScene
        && prevScene.route.routeName === 'Splash'
        && nextScene.route.routeName === 'SocialLogin') {
        return fadeIn(100);
      }

    return fromRight(500);
  }
   
  const AppNavigator = createStackNavigator({
    SocialLogin: { screen: SocialLogin },
    List: { screen: List },
    Menu: { screen: Menu },
    Productos: { screen: Productos },
    DetallesProd: { screen: DetallesProd },
    Registration: { screen: Registration },
    Login: { screen: Login },
    Recovery: { screen: Recovery },
    Direccio: { screen: Direccio },
    Pago: { screen: Pago },
    Fromcreditcard: { screen: Fromcreditcard },
    Myads: { screen: Myads },
    Review: { screen: Review },
    Account: { screen: Account },
    Listadedeseos: { screen: Listadedeseos },
    Ordenes: { screen: Ordenes },
    OrdenesDitails: { screen: OrdenesDitails },
    Pedido: { screen: Pedido },
    PedidoDitails: { screen: PedidoDitails },
    Payment: { screen: Payment },
    Cupon: { screen: Cupon},
    Error: { screen: Error},
    Exito: { screen: Exito },
    ReviewClient: { screen: ReviewClient},
    Reportproblen: { screen: Reportproblen},
    PublicProfile: { screen: PublicProfile},
    Filtros: { screen: Filtros},
    Notification: {screen: Notification},
    Condicionesdeuso: {screen: Condicionesdeuso},
    Splash: {screen: Splash},
    Regalo: {screen: Regalo},
    Frompaypal: { screen: Frompaypal},
    Agenda: { screen: Agenda},
    Categorias: { screen: Categorias },
    Mapa: { screen: Mapa},
    Confirmaemail: { screen: Confirmaemail}
  }, {
    transitionConfig: (nav) => handleCustomTransition(nav),
    initialRouteName: 'Splash',
    headerMode: 'null'
  })
   

export default createAppContainer(AppNavigator);