import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import Avatar from '../../common/assets/ff275e983f23de0f6ece0529a1a7eadc20b521d1.png';

export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <View style={{ width: '100%', height: 225, marginTop: 20, alignContent: "center" }}>
                <Image style={{ width: '100%', height: 160, borderRadius: 5 }} source={{ uri: 'https://images.unsplash.com/photo-1554995207-c18c203602cb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80' }} />
                <Image style={styles.avatar} source={Avatar} />
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 16, marginBottom: 20, marginTop: -10 }}>Pintura y decoracion</Text>
                    <View style={{ alignContent: 'flex-end', marginLeft: 'auto', }}>
                        <Text style={{ fontFamily: 'Helvetica', fontWeight: 'bold', fontSize: 14, color: '#95ca3e', marginTop: 10 }}>15€ /Hora</Text>
                    </View>
                </View>
                <Text style={{ marginTop: -15, color: '#999', fontFamily: 'Helvetica' }}>
                    <Icon name="star" size={15} color="#FFCE4E" /> 4.5 de (45) Valoraciones
                                    </Text>
                <Text style={{ color: '#999', fontFamily: 'Helvetica' }}>
                    · Limpieza · <Icon name="map-marker" size={15} color="#95ca3e" /> Burgos
                        </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    avatar: {
        marginTop: -25,
        width: 40,
        height: 40,
        marginLeft: 'auto',
        borderRadius: 20,
        marginRight: 10,
        borderWidth: 2,
        borderColor: '#d6d7da',
    },
});
