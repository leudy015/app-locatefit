import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, FlatList, ImageBackground, Button, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import dataList from './data';
import Header from "../../common/header.png";
import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import { Badge } from 'react-native-elements';
import Headers from "../../common/headers.png";
import ProductoSmall from './productSmall';
import Productolargue from './productolarge';
import Category from './category';

export default class List extends Component {

    state = {
        isModalVisible: false
    };

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 250 }}>
                    <View style={{ justifyContent: 'center', marginTop: 40 }}>

                        <Animatable.Text style={{ width: '100%', color: '#FFFFFF', paddingLeft: 12 }} animation="bounceInDown" duration={3000}>Ubicación actual</Animatable.Text>

                    </View>
                    <View style={{ width: '100%', flexDirection: 'row', paddingBottom: 15 }}>
                        <TouchableOpacity>
                            <Animatable.Text style={{ width: '100%', color: '#FFFFFF', fontSize: 18, paddingLeft: 12, marginRight: 5 }} animation="bounceInDown" duration={2000}>Burgos <Icon name="chevron-down" size={16} color="#95ca3e" /></Animatable.Text>
                        </TouchableOpacity>


                        <View style={{ width: '52%', textAlign: 'right', alignItems: 'flex-end', justifyContent: 'center', marginBottom: 15, marginLeft: 'auto', marginRight: 12 }}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Menu')}>
                                <Icon name="user-circle" size={22} color="#95ca3e" />
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: -2, right: -2 }}
                                />
                            </TouchableOpacity>
                        </View>


                    </View>
                    <View style={{ width: '100%', paddingVertical: 5, paddingHorizontal: 12 }}>
                        <View style={{ backgroundColor: '#fff', width: '100%', borderRadius: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                            <View style={{ width: '10%', justifyContent: 'center' }}>
                                <Icon name="search" size={20} color="grey" />
                            </View>
                            <TextInput
                                style={{
                                    width: "76%",
                                    height: 35,
                                    borderRadius: 4,
                                    marginTop: 0,
                                    color: '#000'
                                }}
                                placeholderTextColor='grey'
                                placeholder="¿Que necesitas?"
                            />
                            <TouchableOpacity onPress={() => navigation.navigate('Filtros')}
                                style={{ width: '10%', justifyContent: 'center', alignItems: 'flex-end' }}>

                                <Icon name="filter" size={20} color="#95ca3e" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>

                <View style={styles.body}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18, marginBottom: 20}}>Categorías</Text>
                        </View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <TouchableOpacity onPress={() => navigation.navigate('Productos')}>
                               <Category />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('Productos')}>
                               <Category />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('Productos')}>
                               <Category />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('Productos')}>
                               <Category />
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
  

                    <View style={{marginTop: 30}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18, marginBottom: 20}}>Destacados</Text>
                            <Text style={{fontFamily: 'Helvetica', fontSize: 14, alignContent: 'flex-end', marginLeft: 'auto', color: '#95ca3e'}}>Ver todos</Text>
                        </View>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                        </ScrollView>
                    </View>

                    <View style={{marginTop: 30}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18, marginBottom: 20}}>Novedades de Locatefit</Text>
                            <Text style={{fontFamily: 'Helvetica', fontSize: 14, alignContent: 'flex-end', marginLeft: 'auto', color: '#95ca3e'}}>Ver todos</Text>
                        </View>
                        
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                        </ScrollView>
                    </View>

                    <View style={{marginTop: 30}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18, marginBottom: 20}}>Mejor Valorado</Text>
                            <Text style={{fontFamily: 'Helvetica', fontSize: 14, alignContent: 'flex-end', marginLeft: 'auto', color: '#95ca3e'}}>Ver todos</Text>
                        </View>
                        
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                        </ScrollView>
                    </View>

                    <View style={{marginTop: 30}}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18, marginBottom: 20}}>Cerca de ti</Text>
                            <Text style={{fontFamily: 'Helvetica', fontSize: 14, alignContent: 'flex-end', marginLeft: 'auto', color: '#95ca3e'}}>Ver todos</Text>
                        </View>
                        
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <ProductoSmall />
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                    
                    <View style={{marginTop: 30, marginBottom: 70}}>
                    <View style={{flexDirection: 'row'}}>
                            <Text style={{fontWeight: 'bold', fontFamily: 'Helvetica', fontSize: 18,}}>Recomendados para ti</Text>
                            <Text style={{fontFamily: 'Helvetica', fontSize: 14, alignContent: 'flex-end', marginLeft: 'auto', color: '#95ca3e'}}>Ver todos</Text>
                        </View>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productolargue />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productolargue />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productolargue />
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#1c3643',
    },

    header1: {
        marginTop: 'auto',
        width: '100%',
        height: 80
    },

    body: {
        width: "100%",
        height: 'auto',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 250
    },
});




