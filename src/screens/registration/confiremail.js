import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';
import Headers from "../../common/headers.png";
import Logo from './email.png'



export default class Registration extends Component {


    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 200 }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="chevron-left" size={22} color="#95ca3e" /></Text>
                </ImageBackground>
                <ScrollView showsHorizontalScrollIndicator={false}>
                    <View style={styles.body}>

                        <Animatable.Text style={{ color: '#1c3643', fontSize: 24, fontWeight: '400', marginBottom: 20, textAlign: "center", marginTop: 20 }} animation="bounceInDown" duration={3000}>Verifica tu email</Animatable.Text>
                        <Animatable.Text animation="bounceInDown" duration={2000} style={{ fontWeight: '200', color: "#aaa", textAlign: "center", marginBottom: 0, padding: 30 }}>
                            Ve a tu bandeja de entrada, te hemos dejado un enlace para verificar tu email
                    </Animatable.Text>

                        <Image
                            style={{ width: 330, height: 160 }}
                            source={Logo}
                        />
                        <View>
                            <Text style={{ width: '100%', marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                ¿Ya has verificado tu Email?
                        </Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}
                            style={{
                                width: "70%",
                                height: 40,
                                backgroundColor: '#1c3643',
                                borderRadius: 6,
                                justifyContent: "center",
                                alignItems: "center",
                                marginVertical: 10,
                            }}
                        >
                            <Text style={{ fontSize: 18, color: 'white' }}>Iniciar sesión</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },


    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#1c3643',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },

    input: {
        marginBottom: 25,
        width: '100%'
    }
});
