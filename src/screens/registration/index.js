import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, ImageBackground, TouchableOpacity } from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Header from "../../common/header.png";
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';
import Headers from "../../common/headers.png";



export default class Registration extends Component {
    state = {
        email: null,
        username: null,
        password: null,
        errors: [],
        loading: false,
    }

    handleSignUp() {
        const { navigation } = this.props;
        const { email, username, password } = this.state;
        const errors = [];

        Keyboard.dismiss();
        this.setState({ loading: true });

        // check with backend API or with some static data
        if (!email) errors.push('email');
        if (!username) errors.push('username');
        if (!password) errors.push('password');

        this.setState({ errors, loading: false });

        if (!errors.length) {
            Alert.alert(
                'Bienvenido!',
                'Tu cuenta ha sido creada correctamente',
                [
                    {
                        text: 'Continuar', onPress: () => {
                            navigation.navigate('Inicio')
                        }
                    }
                ],
                { cancelable: false }
            )
        }
    }



    render() {
        const { navigation } = this.props;
        const { loading, errors } = this.state;
        const hasErrors = key => errors.includes(key) ? styles.hasErrors : null;
        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 200 }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="chevron-left" size={22} color="#95ca3e" /></Text>
                </ImageBackground>
                <ScrollView
                    showsHorizontalScrollIndicator={false}>
                    <View style={styles.body}>
                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Animatable.Text style={{ color: '#1c3643', fontSize: 24, fontWeight: '400', marginBottom: 30, marginTop: 20 }} animation="bounceInDown" duration={3000}>Regístrate</Animatable.Text>

                        </View>
                        <View style={styles.input}>
                            <Icon name="user" size={22} style={styles.icons} color="#1c3643" />
                            <InputField
                                placeholder="Nombre completo"
                                defaultValue={this.state.username}
                                onChangeText={text => this.setState({ username: text })}
                                error={hasErrors('username')}

                            />
                        </View>

                        <View style={styles.input}>
                            <Icon name="check" size={22} style={styles.icons} color="#1c3643" />
                            <InputField
                                placeholder="Usuario"
                                defaultValue={this.state.username}
                                onChangeText={text => this.setState({ username: text })}
                                error={hasErrors('username')}

                            />
                        </View>

                        <View style={styles.input}>
                            <Icon name="envelope" size={22} style={styles.icons} color="#1c3643" />
                            <InputField placeholder="Correo electrónico" />
                        </View>

                        <View style={styles.input}>
                            <Icon name="lock" size={22} style={styles.icons} color="#1c3643" />
                            <InputField
                                placeholder="Contraseña"
                                secure
                                error={hasErrors('password')}
                                defaultValue={this.state.password}
                                onChangeText={text => this.setState({ password: text })}

                            />

                        </View>

                        <TouchableOpacity
                            onPress={() => navigation.navigate('Confirmaemail')}
                            style={{
                                width: "70%",
                                height: 40,
                                backgroundColor: '#1c3643',
                                borderRadius: 6,
                                justifyContent: "center",
                                alignItems: "center",
                                marginVertical: 10,
                            }}
                        >
                            <Text style={{ fontSize: 18, color: 'white' }}>Regístrarme</Text>
                        </TouchableOpacity>
                        <View
                            style={[styles.textView], {
                                marginVertical: 40,
                            }}>
                            <Text style={{ fontWeight: '200' }}>
                                ¿Tienes una cuenta?
                            <Text style={{ color: '#1c3643', fontWeight: "bold", fontSize: 18 }} onPress={() => navigation.navigate('Login')}> Inicia sesión</Text>
                            </Text>
                        </View>
                        <View
                            style={[styles.textView], {
                                marginBottom: 5,
                                marginTop: 0
                            }}>
                            <Text style={{ fontWeight: '200', color: '#d7d7d7', textAlign: 'center', padding: 10 }}>
                                Continuar implica que has leído y aceptado los
                            <Text style={{ color: '#38aef6' }}> Términos y condiciones</Text>
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    header1: {
        marginTop: 55,
        width: '100%',
        height: 80
    },

    icons: {
        marginTop: 17

    },

    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#1c3643',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Logo: {
        height: 33,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "85%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 25,
        width: '90%',
        flexDirection: 'row'
    }
});
