import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, ImageBackground, Image,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Headers from "../../common/headers.png";
import Productlargue from './../List/productolarge';





export default class List extends Component {

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 200, alignItems: 'center' }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="times" size={22} color="#95ca3e" /></Text>
                    <View style={{
                        width: '90%',
                        height: 100,
                        backgroundColor: '#fff',
                        marginTop: 30,
                        borderRadius: 15,
                        shadowColor: '#ddd',
                        shadowOffset: { width: 0, height: 1 },
                        shadowOpacity: 0.5,
                        shadowRadius: 8,
                        elevation: 5,
                        padding: 10
                    }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ fontFamily: 'Helvetica', fontSize: 24, fontWeight: 'bold', color: '#474747', marginTop: 5, marginLeft: 10 }}>LIMPIEZA</Text>
                                <Text style={{ color: '#999', fontFamily: 'Helvetica', marginLeft: 10, marginTop: 5 }}>
                                    <Icon name="map-marker" size={15} color="#95ca3e" /> Burgos
                            </Text>
                                <Text style={{ color: '#999', fontFamily: 'Helvetica', marginLeft: 10, marginTop: 5 }}>
                                    (234) Resultado de la burqueda
                            </Text>
                            </View>

                            <View style={{ alignContent: 'flex-end', marginLeft: 'auto', marginRight: 10, marginTop: 30 }}>
                                <Text>
                                    <Icon onPress={() => navigation.navigate('Filtros')} name="filter" size={24} color="#95ca3e" />
                                </Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
                
                <View style={styles.body}>
                   
                        <ScrollView style={{marginBottom: 90}} showsVerticalScrollIndicator={false}>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productlargue />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productlargue />
                                </TouchableOpacity>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                <Productlargue />
                                <TouchableOpacity onPress={() => navigation.navigate('DetallesProd')}>
                                </TouchableOpacity>
                                <Productlargue />
                                </TouchableOpacity>
                            
                        </ScrollView>

                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    whilist: {
        position: "absolute",
        marginLeft: 10,
        marginTop: 10,
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 7,
        borderRadius: 30
    },
    
    body: {
        width: "95%",
        paddingTop: 20,
        marginBottom: 110,
    },

});