import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem, Header } from 'react-native-elements'


const list = [
    {
        name: 'Ofrecido por: Leudy Martes',
        avatar_url: 'https://www.canariasnoticias.es/sites/default/files/2019/01/61587.jpg',
        subtitle: 'Servicio de Fontanería'
    },

]


export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                    }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null), }}
                    centerComponent={{ text: 'FINALIZAR LA CONTRATACIÓN', style: { color: '#1c3643' } }}
                />



                <View style={styles.body}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ marginTop: 20 }}>
                            {
                                list.map((l, i) => (
                                    <ListItem
                                        topDivider
                                        key={i}
                                        leftAvatar={{ source: { uri: l.avatar_url } }}
                                        title={l.name}
                                        subtitle={l.subtitle}
                                        bottomDivider

                                    />
                                ))
                            }
                        </View>

                        <View style={{ marginTop: 20, borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: '#ddd', borderTopColor: '#ddd', paddingBottom: 10 }}>
                            <View style={{ flexDirection: 'column', alignItems: 'flex-start', marginRight: 10, marginLeft: 15 }}>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Precio unitario
                                </Text>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Cantidad
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Descuento
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Total
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    Total a pagar
                            </Text>
                            </View>

                            <View style={{ flexDirection: 'column', alignItems: 'flex-end', marginRight: 15, marginLeft: 15, marginTop: -130 }}>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    1
                                </Text>
                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    0,00€
                                </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                            </Text>

                                <Text style={{ fontSize: 14, color: "#555", marginTop: 10 }}>
                                    99,99€
                            </Text>
                            </View>
                        </View>

                        <View style={{ alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ddd', paddingBottom: 20 }}>
                            <Text style={{ fontWeight: '200', marginTop: 30 }}>
                                ¿Tienes un cupón?
                            </Text>
                            <TouchableOpacity style={styles.cardtarjeta} onPress={() => navigation.navigate('Cupon')}>

                                <Text>
                                    <Icon name="percent" size={16} color="#95ca3e" />   Añadir cupón
                                </Text>
                            </TouchableOpacity >
                        </View>

                        <View style={{ alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ddd', paddingBottom: 20 }}>
                            <Text style={{ fontWeight: '200', marginTop: 30 }}>
                                Añadir descripción de la tarea
                            </Text>
                            <View style={styles.input}>
                                <InputField
                                    placeholder="Descripción de la tarea"
                                />
                            </View>


                        </View>


                        <View style={{ padding: 15, borderBottomWidth: 1, borderBottomColor: '#ddd', }}>
                            <Text style={{ color: '#95ca3e', fontSize: 18, fontWeight: '400', marginBottom: 10, }}>Donde quieres que vallamos</Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                Leudy Martes
                        </Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                Calle Francisco Sarmiento No. 13 8A
                        </Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                Burgos
                        </Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                Burgos
                        </Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                09005
                        </Text>

                            <Text style={{ fontWeight: '200', marginBottom: 5, color: "#555" }}>
                                689 351 592
                        </Text>

                            <View style={{ alignItems: 'center' }}>
                                <TouchableOpacity style={styles.cardtarjeta} onPress={() => navigation.navigate('Direccio')}>
                                    <Text><Icon name="plus" size={16} color="#95ca3e" />   Añadir dirección</Text>
                                </TouchableOpacity>
                            </View>


                        </View>




                        <View style={{ alignItems: 'center', marginBottom: 100, marginTop: 50 }}>

                            <TouchableOpacity style={styles.cardtarjeta} onPress={() => navigation.navigate('Fromcreditcard')}>
                                <Text>
                                    <Icon name="credit-card" size={16} color="#95ca3e" />  Pagar con Tarjeta
                                </Text>
                            </TouchableOpacity >

                            <TouchableOpacity style={styles.cardtarjeta} onPress={() => navigation.navigate('Frompaypal')}>
                                <Text>
                                    <Icon name="paypal" size={16} color="#95ca3e" />  Pagar con Paypal
                                </Text>
                            </TouchableOpacity >


                        </View>

                    </ScrollView>


                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '85%',
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",


    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 10,
        width: '95%',
        flexDirection: 'row'
    }
});
