import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ListItem, SocialIcon, Badge } from 'react-native-elements'
import Headerparallax from './../../components/headerparallax';
import Fotoperfil from './../../../leudy.png'


{/*
   <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 90, marginLeft: -330, marginBottom: 30 }} onPress={() => navigation.goBack(null)}><Icon name="times" size={22} color="#95ca3e" /></Text>
<Animatable.Text duration={4000} animation="swing" style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: -50, marginLeft: 330, marginBottom: 30 }} onPress={() => navigation.navigate('Notification')}><Icon name="bell" size={22} color="#95ca3e" /></Animatable.Text>
        
 */}

export default class Menu extends Component {

  render() {

    const { navigation } = this.props;

    return (
      <Headerparallax name='Leudy Martes' descripcion='Miembro desde 29 oct 2019' back={
        <Icon name="times" size={18} color="#95ca3e" onPress={() => navigation.goBack(null)} />
      }
        avatar={Fotoperfil}
        component={
          <View>

            <View>

              <ListItem
                title="Mi cuenta"
                subtitle='Edita tu datos y foto del perfil'
                onPress={() => navigation.navigate('Account')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/usuario-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Dirección"
                subtitle='Añade una dirección'
                onPress={() => navigation.navigate('Direccio')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/ubicacion-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title={'Mi lista de deseos'}
                subtitle='Aquí tienes tus servicios que le diste a me gusta'
                badge
                onPress={() => navigation.navigate('Listadedeseos')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/corazon-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Notificaciones"
                subtitle='Echale un vistazo a lo nuevo'
                badge
                onPress={() => navigation.navigate('Notification')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/notificaciones-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Mis cupones de descuento!"
                subtitle='Aquí tienes los cupones disponibles'
                onPress={() => navigation.navigate('Regalo')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/regalo-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem

                title="Pedidos"
                subtitle='Gestiona tus pedidos'
                onPress={() => navigation.navigate('Pedido')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/pedidos-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Ordenes"
                subtitle='Gestiona tus ordenes'
                badge
                onPress={() => navigation.navigate('Ordenes')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/ordenes-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Mis pagos"
                subtitle='Añade método de pago para retiros de ventas'
                onPress={() => navigation.navigate('Pago')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/pagos-01.png' } }}
                bottomDivider
                chevron
              />

              <ListItem
                title="Privacidad"
                subtitle='Aquí esta todo lo relacionado con tu privacidad'
                onPress={() => navigation.navigate('Condicionesdeuso')}
                leftAvatar={{ source: { uri: 'https://menuicnonos.s3.eu-west-3.amazonaws.com/privacidad-01.png' } }}
                chevron
                bottomDivider
              />

              <View style={{ marginBottom: 30, marginTop: 30}}>
              <ListItem
                title='Permitir notificaciones'
                switch
              />
              </View>


              <View style={{ flexDirection: 'row', marginBottom: 50 }}>
                <SocialIcon type='facebook' />
                <SocialIcon type='twitter' />
              </View>


            </View>

          </View>

        }
      />
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  icons: {
    marginTop: 0,

  },

  header1: {
    marginTop: 13,
    width: '100%',
    height: 80
  },

  header: {
    width: "100%",
    height: '30%',
    backgroundColor: '#1c3643',
    justifyContent: 'center',
    alignItems: 'center',

  },
  body: {
    width: "100%",
    height: '70%',
    marginTop: 50
  },

  Logo: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: -50

  },

  item: {
    width: "100%",
    height: 40,
    backgroundColor: "#fff",
    marginBottom: 10,
    paddingLeft: 30,
    fontSize: 18,

  },

});

