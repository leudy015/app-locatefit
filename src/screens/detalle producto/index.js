import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, FlatList, ScrollView, Button, } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Avatar } from 'react-native-elements';
import Escudo from "../../common/escudo.png";
import Headerparallax from './../../components/headerparallaxDetallesdelproducto';
import Fotoperfil from './../../../leudy.png'
import ImagenPro from './../../../fontaneria2.jpg'
import Comentario from './comentarios';
import Mapa from './mapa';


export default class Detallesdelpro extends Component {
  
  render() {

    const { navigation } = this.props;

    return (
      <Headerparallax name='Leudy Martes'
        descripcion={<Text style={{ fontSize: 14, fontWeight: '400', color: 'white' }}>
          <Icon name="star" size={18} color="#FFCE4E" /> 4.6 Valoración  (24) . Fontanería .  <Icon name="map-marker" size={18} color="#95ca3e" /> Burgos
        </Text>}
        back={<Icon name="times" size={18} color="#95ca3e" onPress={() => navigation.goBack(null)}/>}
        back2={<Icon name="share" size={18} color="#95ca3e" />}
        avatar={Fotoperfil}
        imagen={ImagenPro}
        component={<View style={styles.carddetalles} >

          <View style={{ width: '100%', padding: 15, flexDirection: 'row' }}>
            <Text style={{ fontSize: 20, fontWeight: '400', color: '#000', padding: 3 }}>
              Servicio de fontanería
            </Text>
            <Text style={{ fontSize: 24, fontWeight: '700', color: '#95ca3e', marginLeft: 'auto' }}>
              85€
            </Text>
          <Text style={{ fontSize: 16, fontWeight: '400', color: '#999', marginTop: 5,  textAlign: 'left', }}>
             /Hora
          </Text>
          </View>
          <View style={{ width: '100%', paddingLeft: 15, paddingRight: 15 }}>
            <Text style={{ fontSize: 16, fontWeight: '200', color: '#aaa', marginTop: 5 }}>
                Lorem Ipsum es simplemente un texto ficticio de la industria de impresión y
                composición tipográfica. Lorem Ipsum.
            </Text>
          </View>

          <View style={{flexDirection: 'row', marginLeft: 15, marginTop: 20}}>
          <Icon style={{paddingRight: 5}} name="star" size={18} color="#FFCE4E" />
          <Icon style={{paddingRight: 5}} name="star" size={18} color="#FFCE4E" />
          <Icon style={{paddingRight: 5}} name="star" size={18} color="#FFCE4E" />
          <Icon style={{paddingRight: 5}} name="star" size={18} color="#FFCE4E" />
          <Icon style={{paddingRight: 5}} name="star" size={18} color="#FFCE4E" />
          </View>

          <View style={{ width: '100%', justifyContent: 'flex-start' }}>

            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20, marginLeft: 15 }}>
              <View style={styles.qubo}>
                <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5}}>
                  <Icon name="history" size={20} color="#3e5c0a" />
                </Text>
                <Text style={{ fontSize: 12,fontWeight: '400', color: '#3e5c0a',textAlign: 'center' }}>
                  Tiempo limitado!
                  </Text>
              </View>

              <View style={styles.qubo}>
                <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5}}>
                  <Icon name="eye" size={20} color="#3e5c0a" />
                </Text>
                <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                  +{Math.floor(Math.random() * (50, 70) + 20)} visitas
                  </Text>
              </View>

              <View style={styles.qubo}>
                <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 5}}>
                  <Icon name="star" size={20} color="#3e5c0a" />
                </Text>
                <Text style={{ fontSize: 12, fontWeight: '400', color: '#3e5c0a', textAlign: 'center' }}>
                  (24) Opiniones
                  </Text>
              </View>

            </View>
          </View>

          <View style={styles.navigatebajo}>
              <Icon style={{marginLeft: 15, marginRight: 20}} name="minus-circle" size={28} color="#95ca3e" />
              <Text style={{ fontSize: 18, alignItems: "center", color: '#202020', justifyContent: "center", marginRight: 5 }}>1</Text>
              <Icon style={{marginLeft: 15, marginRight: 20}} name="plus-circle" size={28} color="#95ca3e" />
              <Icon style={{marginLeft: 15, marginRight: 20}} name="heart" size={28} color="#ddd" />
              <TouchableOpacity
                onPress={() => navigation.navigate('Agenda')}
                style={{
                  width: "50%",
                  height: 40,
                  backgroundColor: '#95ca3e',
                  borderRadius: 50,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Contratar profesional</Text>
              </TouchableOpacity>
             
            </View>

          <View style={{ width: '100%', marginTop: 20, }}>
              <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#444', }}>
                Perfil del profesional
              </Text>
              <View style={{ width: '100%', height: 85, flexDirection: 'row', marginTop: 20, color: "464646", padding: 17 }}>
                <Avatar
                  size="medium"
                  rounded
                  source={Fotoperfil}
                />
              <View>
                <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#95ca3e', }}>
                  Leudy Martes <Icon name="check-circle" size={20} color="rgb(82, 196, 26)" />
                </Text>
                <Text style={{ fontSize: 12, marginLeft: 15, fontWeight: '300', color: '#333', }}>
                  Fontanero
              </Text>
              </View>
              <Text onPress={() => navigation.navigate('PublicProfile')} style={{ fontSize: 14, color: "#95ca3e", marginTop: 18, textAlign: 'right', alignItems: 'flex-end', marginLeft: 'auto' }}>
                Más detalles
            </Text>
            </View>
          </View>

          <View style={{ width: '100%' }}>

            <View style={{ width: '80%', height: 100, backgroundColor: "#fff", color: "464646", padding: 17, flexDirection: 'row' }}>
              <Text style={{ fontSize: 14, color: "#aaa" }}>
                Para proteger tus pagos, nunca trasfiera dinero ni te comuniques fuera de la página o la aplicación de Locatefit.
                            </Text>

              <Image style={{ width: 80, height: 80, marginLeft: 10, marginTop: -15 }}
                source={Escudo}
              />
            </View>
          </View>

          <View style={{ width: '100%', marginLeft: 15, marginTop: 10, marginRight: 15 }}>
            <Text style={{ fontSize: 20, fontWeight: '300', color: '#444', }}>
              Ubicación
            </Text>
              <Mapa />
          </View>

          <View style={{ width: '100%', marginTop: 20, marginBottom: 50 }}>
            <Text style={{ fontSize: 20, marginLeft: 15, fontWeight: '300', color: '#444', }}>
              Opiniones de clientes
            </Text>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <Comentario />
              <Comentario />
              <Comentario />
              <Comentario />
              <Comentario />
              <Comentario />
              <Comentario />
              <Comentario />
            </ScrollView>
          </View>
          
        </View>
        

        }
      />
    );
  }
}

const styles = StyleSheet.create({

  header: {
    width: "100%",
    height: 355,
  },

  carddetalles: {
    backgroundColor: 'white',
    marginTop: -30,
    borderRadius: 20,
  },

  qubo: {
    flexDirection: 'column', 
    backgroundColor: '#CDD89B', 
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 20,
    color: '#4B4B3A'
  },

  navigatebajo:{
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    height: 80,
    marginTop: 20,
  }

});