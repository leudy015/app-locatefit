import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import Logo from '../../common/assets/logo.png';
import Splashback from './splash.png'

export default class Splash extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('SocialLogin')
            }, 4000)
           }
    render() {
        return (
            <ImageBackground source={Splashback} style={styles.container}>
                    <Image
                        style={{ width: 270, height: 50 }}
                        source={Logo}
                    />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Logo: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'helvetica'
    },
});
