import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Rating, ListItem, Header } from 'react-native-elements';

const list = [
    {
        name: 'Servicio de Fontanería',
        avatar_url: 'https://www.canariasnoticias.es/sites/default/files/2019/01/61587.jpg',
        subtitle: 'Burgos'
    },
]



export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
               <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'REVISIÓN', style: { color: '#1c3643' } }}
                />



                <View style={styles.body}>
                    <ScrollView>
                        <View>
                            {
                                list.map((l, i) => (
                                    <ListItem
                                        key={i}
                                        leftAvatar={{ source: { uri: l.avatar_url } }}
                                        title={l.name}
                                        subtitle={l.subtitle}
                                        bottomDivider
                                    />
                                ))
                            }
                        </View>
                        <Text style={{ textAlign: "center", color: "#999", padding: 15, marginTop: 10 }}>Recientemente uno de nuestros profesionales te ha realiza un servicio valora su experiencia para ayudar a otros usuarios.</Text>
                        <View style={{ textAlign: "center", color: "#444", fontSize: 24 }}>
                            <Text style={{ textAlign: "center", color: "#444", fontSize: 20, padding: 15 }}>
                                Puntua nuestro profesional con una valoración
                            </Text>
                            <Rating showRating fractions={1} startingValue={0} />
                        </View>



                        <View style={{ alignItems: "center", textAlign: "center", justifyContent: "center", }}>
                            <View style={styles.input}>
                                <InputField placeholder="Escribe un comentario" />
                            </View>

                         
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Payment')}
                                style={{
                                    width: "60%",
                                    height: 40,
                                    backgroundColor: '#95ca3e',
                                    borderRadius: 5,
                                    bottom: 25,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginVertical: 10,
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Enviar valoración</Text>
                            </TouchableOpacity>
                        </View>
                    

                        <View style={{ alignItems: "center", textAlign: "center", justifyContent: "center", marginBottom: 100 }}>
                            <Text style={{ textAlign: "center", color: "#444", fontSize: 20, padding: 15 }}>
                                Reportar un problema con el servicio.
                            </Text>
                            <Text style={{ textAlign: "center", color: "#999", padding: 15, }}>
                                Si ha tenido algun problema con el servicio brindado puedes reportalo para que tenga la mejor experiencia en nuestra comunidad.
                            </Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Reportproblen')}
                                style={{
                                    width: "60%",
                                    height: 40,
                                    backgroundColor: 'red',
                                    borderRadius: 5,
                                    bottom: 5,
                                    Top: 30,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginVertical: 10,
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Reportar un problema</Text>
                            </TouchableOpacity>
                        </View>


                    </ScrollView>



                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '85%',
        marginTop: 50

    },


    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 10,
        width: '95%',
        flexDirection: 'row',
        marginTop: 70,
        marginBottom: 40

    }
});
