import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem } from 'react-native-elements'
import  {Header}  from 'react-native-elements';
const list = [
    {
        name: 'Peluquería',
        
    },
    {
        name: 'Fontaneria',

    },
    {
        name: 'Pintura',
       
    },
    {
        name: 'Limpieza',
       
    },


]




export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                 <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'CATEGORÍAS', style: { color: '#1c3643' } }}
                />



                <View style={styles.body}>
                    <ScrollView>
                        <View>
                            {
                                list.map((l, i) => (
                                    <ListItem
                                        key={i}
                                        onPress={() => navigation.navigate('Filtros')}
                                        title={l.name}
                                        bottomDivider
                                    />
                                ))
                            }
                        </View>

                    </ScrollView>


                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 10,
        width: '95%',
        flexDirection: 'row'
    }
});
