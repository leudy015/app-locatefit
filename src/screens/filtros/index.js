import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Rating, ListItem, Icon } from 'react-native-elements';
import Slider from 'react-native-slider';
import RNPickerSelect from 'react-native-picker-select';

export default class Registration extends Component {
    render() {

        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                    }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null), }}
                    centerComponent={{ text: 'FILTROS', style: { color: '#1c3643' } }}
                />

                <View style={styles.body}>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}>
                        <View>
                           
                            <ListItem
                                onPress={() => navigation.navigate('Categorias')}
                                title='Categorías'
                                subtitle='Fontanería'
                                bottomDivider
                                chevron
                            />

                              <ListItem
                                onPress={() => navigation.navigate('Mapa')}
                                title='Ubicación'
                                subtitle='Burgos'
                                bottomDivider
                                chevron
                            />
                        </View>

                        <View style={{ margin: 20, borderBottomWidth: 0.5, borderBottomColor: '#ddd', marginTop: 20, paddingBottom: 10 }}>
                          
                             <Text style={{ textAlign: 'left', color: '#666', fontSize: 18, marginTop: 10 }}>Filtrar por precio</Text>
                            <Slider
                                value={1000}
                                maximumValue={1000}
                                minimumValue={1}
                                minimumTrackTintColor='#95ca3e'
                                thumbTintColor='#95ca3e'
                            />
                            <Text style={{ textAlign: 'right', color: '#666' }}>100€</Text>

                        </View>

                        <View style={{ margin: 20, borderBottomWidth: 0.5, borderBottomColor: '#ddd', paddingBottom: 10 }}>
                        <Text style={{ textAlign: 'left', color: '#666', fontSize: 18, marginTop: 10 }}>Filtrar por valoración</Text>
                            <Slider
                                value={5}
                                maximumValue={5}
                                minimumValue={1}
                                minimumTrackTintColor='#95ca3e'
                                thumbTintColor='#95ca3e'
                            />
                             <Text style={{ textAlign: 'right', color: '#666' }}>5</Text>
                        </View>


                        <View style={{flexDirection: 'row', width: '100%', marginTop: 50}}>

                        <View style={{ width: '40%', margin: 20, paddingBottom: 40, justifyContent: 'center', marginTop: 20 }}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Productos')}
                                style={{
                                    width: "100%",
                                    height: 40,
                                    backgroundColor: '#95ca3e',
                                    borderRadius: 50,
                                    position: 'absolute',
                                    bottom: 25,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginVertical: 10,
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Aplicar</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ width: '40%', margin: 20, paddingBottom: 40, justifyContent: 'center', marginTop: 10, marginRight:0 }}>
                            <TouchableOpacity
                                onPress={() => navigation.goBack(null)}
                                style={{
                                    width: "100%",
                                    height: 40,
                                    backgroundColor: '#ddd',
                                    borderRadius: 50,
                                    position: 'absolute',
                                    bottom: 25,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginVertical: 10,
                                }}
                            >
                                <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    </ScrollView>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    slider: {
        height: 40,
        margin: 20,
    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '85%',
        marginTop: 50

    },


    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        width: '95%',
        marginBottom: 10,
        padding: 20

    }
});
