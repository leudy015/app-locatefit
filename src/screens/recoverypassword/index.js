import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity} from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Header from "../../common/header.png";
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';
import Headers from "../../common/headers.png";

const VALID_EMAIL = "contact@react-ui-kit.com";


export default class Recoverypassword extends Component {
    state = {
        email: VALID_EMAIL,
        errors: [],
        loading: false,
    }

    handleForgot() {
        const { navigation } = this.props;
        const { email } = this.state;
        const errors = [];

        Keyboard.dismiss();
        this.setState({ loading: true });

        // check with backend API or with some static data
        if (email !== VALID_EMAIL) {
            errors.push('email');
        }

        this.setState({ errors, loading: false });

        if (!errors.length) {
            Alert.alert(
                'Password sent!',
                'Please check you email.',
                [
                    {
                        text: 'OK', onPress: () => {
                            navigation.navigate('Login')
                        }
                    }
                ],
                { cancelable: false }
            )
        } else {
            Alert.alert(
                'Error',
                'Please check you Email address.',
                [
                    { text: 'Try again', }
                ],
                { cancelable: false }
            )
        }
    }
    render() {

        const { navigation } = this.props;
        const { loading, errors } = this.state;
        const hasErrors = key => errors.includes(key) ? styles.hasErrors : null;

        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 200 }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="chevron-left" size={22} color="#95ca3e" /></Text>
                </ImageBackground>
                <ScrollView
                    showsHorizontalScrollIndicator={false}>
                    <View style={styles.body}>
                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Animatable.Text style={{ color: '#1c3643', fontSize: 24, fontWeight: '400', marginBottom: 20, textAlign: "center", marginTop: 20 }} animation="bounceInDown" duration={3000}>Recuperar contraseña</Animatable.Text>

                            <Animatable.Text animation="bounceInDown" duration={2000} style={{ fontWeight: '200', color: "#aaa", textAlign: "center", marginBottom: 0, padding: 30 }}>
                                Escríbe tu email y te ayudaremos a cambiar tu contraseña
                        </Animatable.Text>

                        </View>


                        <View style={styles.input}>
                            <Icon name="envelope" marginLeft={10} style={styles.icons} size={22} color="#1c3643" />
                            <InputField
                                placeholder="Correo electrónico"
                                error={hasErrors('email')}
                                defaultValue={this.state.email}
                                onChangeText={text => this.setState({ email: text })}
                            />
                        </View>

                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}
                            style={{
                                width: "70%",
                                height: 40,
                                backgroundColor: '#1c3643',
                                borderRadius: 6,
                                justifyContent: "center",
                                alignItems: "center",
                                marginVertical: 10,
                            }}
                        >
                            <Text style={{ fontSize: 18, color: 'white' }}>Enviadme el enlace</Text>
                        </TouchableOpacity>
                        <View
                            style={[styles.textView], {
                                marginVertical: 40,
                            }}>
                            <Text style={{ fontWeight: '200', color: "#A19D9D" }}>
                                ¿No tienes una cuenta?
                        </Text>
                            <Text style={{ color: '#1c3643', fontWeight: "bold", fontSize: 18, textAlign: "center", marginTop: 30 }} onPress={() => navigation.navigate('Registration')} > Registrate</Text>
                        </View>
                        <View
                            style={[styles.textView], {
                                marginBottom: 5,
                                marginTop: 12
                            }}>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    header1: {
        marginTop: 55,
        width: '100%',
        height: 80
    },

    icons: {
        marginTop: 17

    },

    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#1c3643',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    textView: {
        width: "85%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 30,
        width: '90%',
        flexDirection: 'row'
    }
});