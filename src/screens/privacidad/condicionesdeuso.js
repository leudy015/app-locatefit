/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @format
 */

'use strict';
import  {Header}  from 'react-native-elements';
var React = require('react');
var PropTypes = require('prop-types');
var ReactNative = require('react-native');
var {Linking, StyleSheet, Text, TouchableOpacity, View, ScrollView} = ReactNative;

class OpenURLButton extends React.Component {
  static propTypes = {
    url: PropTypes.string,
  };

  handleClick = () => {
    Linking.canOpenURL(this.props.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.url);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleClick}>
        <View style={styles.button}>
          <Text style={styles.text}>Abrir {this.props.url}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class IntentAndroidExample extends React.Component {
  static title = 'Linking';
  static description = 'Shows how to use Linking to open URLs.';

  render() {
    const {navigation} = this.props
    return (
      <View style={styles.container}>
      <Header
      containerStyle={{ backgroundColor: '#fff',}}
      leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
      centerComponent={{ text: 'POLITICA DE PRIVACIDAD Y USO', style: { color: '#1c3643' } }}
        />
        
        <ScrollView
            showsHorizontalScrollIndicator={false}>
            <View style={styles.body}>
              <OpenURLButton url={'https://www.locatefit.es'} />
              <OpenURLButton url={'https://about.locatefit.es/'} />
              <OpenURLButton url={'https://about.locatefit.es/contacto'} />
              <OpenURLButton url={'https://about.locatefit.es/privacidad'} />
              <OpenURLButton url={'https://about.locatefit.es/condiciones-de-uso'} />
              <OpenURLButton url={'https://about.locatefit.es/cookies'} />
              <OpenURLButton url={'https://about.locatefit.es/preguntas-frecuentes'} />
              <OpenURLButton url={'https://about.locatefit.es/empleo'} />
            </View>
        </ScrollView>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  body:{
    backgroundColor: 'white',
    padding: 10,
    paddingTop: 30,
  },
  button: {
    padding: 10,
    backgroundColor: '#fff',
    marginBottom: 10, 
    borderWidth: 0.5,
    borderColor: '#ddd'
  },
  text: {
    color: '#555',
  },
});

module.exports = IntentAndroidExample;
