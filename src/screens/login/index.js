import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity} from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Header from "../../common/header.png";
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';
import Headers from "../../common/headers.png";


const VALID_EMAIL = "contact@react-ui-kit.com";
const VALID_PASSWORD = "subscribe";


export default class Login extends Component {
    state = {
        email: VALID_EMAIL,
        password: VALID_PASSWORD,
        errors: [],
        loading: false,
    }

    handleLogin() {
        const { navigation } = this.props;
        const { email, password } = this.state;
        const errors = [];

        Keyboard.dismiss();
        this.setState({ loading: true });

        // check with backend API or with some static data
        if (email !== VALID_EMAIL) {
            errors.push('email');
        }
        if (password !== VALID_PASSWORD) {
            errors.push('password');
        }

        this.setState({ errors, loading: false });

        if (!errors.length) {
            navigation.navigate("Browse");
        }
    }
    render() {

        const { navigation } = this.props;
        const { loading, errors } = this.state;
        const hasErrors = key => errors.includes(key) ? styles.hasErrors : null;


        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{ width: '100%', height: 200 }}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="chevron-left" size={22} color="#95ca3e" /></Text>
                </ImageBackground>

                <ScrollView
                    showsHorizontalScrollIndicator={false}>
                    <View style={styles.body}>
                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Animatable.Text style={{ color: '#1c3643', fontSize: 24, fontWeight: '400', marginBottom: 50, textAlign: "center", marginTop: 20 }} animation="bounceInDown" duration={3000}>Iniciar sesión</Animatable.Text>
                        </View>
                        <View style={styles.input}>
                            <Icon name="user" size={22} style={styles.icons} color="#1c3643" />
                            <InputField
                                placeholder="Usuario"
                                defaultValue={this.state.username}
                                onChangeText={text => this.setState({ username: text })}
                                error={hasErrors('username')}

                            />
                        </View>

                        <View style={styles.input}>
                            <Icon name="lock" size={22} style={styles.icons} color="#1c3643" />
                            <InputField
                                placeholder="Contraseña"
                                secure
                                error={hasErrors('password')}
                                defaultValue={this.state.password}
                                onChangeText={text => this.setState({ password: text })}

                            />

                        </View>

                        <TouchableOpacity
                            onPress={() => navigation.navigate('List')}
                            style={{
                                width: "70%",
                                height: 40,
                                backgroundColor: '#1c3643',
                                borderRadius: 6,
                                justifyContent: "center",
                                alignItems: "center",
                                marginVertical: 10,
                            }}
                        >
                            <Text style={{ fontSize: 18, color: 'white' }}>Iniciar sesión</Text>
                        </TouchableOpacity>
                        <View
                            style={[styles.textView], {
                                marginTop: 30,
                                marginBottom: 15
                            }}>
                            <Text style={{ fontWeight: '200', color: "#A19D9D" }}>
                                ¿No tienes una cuenta?
                        </Text>
                        </View>
                        <View style={[styles.textView], {
                            marginBottom: 30,
                        }}>
                            <Text style={{ color: '#1c3643', fontWeight: "bold", fontSize: 18 }} onPress={() => navigation.navigate('Registration')} > Regístrate</Text>
                        </View>
                        <View
                            style={[styles.textView], {
                                marginBottom: 5,
                                marginTop: 12
                            }}>

                            <Text style={{ color: '#1c3643', fontWeight: "bold", fontSize: 18, textAlign: "center", marginBottom: 30 }} onPress={() => navigation.navigate('Recovery')}> ¿Has olvidado tu contraseña?</Text>


                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    header1: {
        marginTop: 55,
        width: '100%',
        height: 80
    },

    icons: {
        marginTop: 17

    },

    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#1c3643',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Logo: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontSize: 26,
        fontWeight: 'bold',
        fontFamily: 'helvetica'
    },
    textView: {
        width: "85%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 30,
        width: '90%',
        flexDirection: 'row'
    },

    login: {
        flex: 1,
        justifyContent: 'center',
    },

    hasErrors: {
        color: "red"

    }
});
