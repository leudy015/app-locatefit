import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Rating, ListItem, Avatar, SocialIcon, Header} from 'react-native-elements';
import Headerparallax from './../../components/headerparallax';
import Fotoperfil from './../../../leudy.png'


export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <Headerparallax name='Leudy Martes' descripcion='Peluquero profesional con más de 10 años de experiencia, en cortes clásico, degradados y muchos otros más.' back={
                <Icon name="times" size={18} color="#95ca3e" onPress={() => navigation.goBack(null)}/>
           }
           avatar={Fotoperfil}

           component={
            <View>
            <View style={styles.input}>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Nombre: Leudy
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Apellidos:  Martes
               </Text >
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Ciudad: Burgos
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Cumpleaños: 15/08/1994
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Pais: España 🇪🇸
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Sitio Web: https://leudybarbers.es
               </Text>
            </View>

            <View style={styles.input}>
                <Text style={{ fontSize: 20, marginBottom: 10 }}>
                    Información profesional
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Profesión:  Web Developer
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Estudio:  Bachiller
               </Text >
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Grado de experiencia:   100
               </Text>
            </View>

            <View style={styles.input}>
                <Text style={{ fontSize: 20, marginBottom: 10 }}>
                    Contacto
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Email:  leudy1521@gmail.com
               </Text>
                <Text style={{ fontSize: 14, marginBottom: 5 }}>
                    Número movil:  692924767
               </Text >

                <View style={{ fontSize: 14, marginBottom: 20, flexDirection: "row", marginTop: 20, }}>
                    <SocialIcon
                        type='facebook'
                    />

                    <SocialIcon
                        type='twitter'
                    />

                    <SocialIcon
                        light
                        type='instagram'
                    />

                    <SocialIcon
                        light
                        type='linkedin'
                    />
                </View>
            </View>
          </View>
           }

      />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '85%',
        marginTop: 50

    },


    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        width: '95%',
        marginBottom: 10,
        padding: 20

    }
});
