import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements';
import { CalendarList } from 'react-native-calendars';



export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                    }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null), }}
                    centerComponent={{ text: 'SELECCIONA HORA Y FECHA', style: { color: '#1c3643' } }}
                />

                <View style={styles.body}>
                    <ScrollView>
                    <View style={{height: '50%'}}>
                        <CalendarList
                            // Enable horizontal scrolling, default = false
                            horizontal={true}
                            // Enable paging on horizontal, default = false
                            // Set custom calendarWidth.
                            calendarWidth={400}

                        />
                    </View>

                    
                
                    <View style={{ width: '100%',  paddingBottom: 10, marginTop: 10, alignItems: 'center' }}>
                    <View style={{height: 150, marginTop: 20, textAlign: "left", }}>
                        <Text style={{fontSize: 18, color: '#999', marginLeft: 20}}>Horas disponibles para este día</Text>
                        
                        <View style={{marginTop: 20, width: '100%', flexDirection: 'row'}}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                    <Text style={{fontSize: 18, color: 'white', textAlign: 'center'}}>10:00</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                    <Text style={{fontSize: 18, color: 'white', textAlign: 'center'}}>11:00</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                    <Text style={{fontSize: 18, color: 'white', textAlign: 'center'}}>12:00</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                    <Text style={{fontSize: 18, color: 'white', textAlign: 'center'}}>13:00</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ color: '#fff', backgroundColor: '#95ca3e', padding: 10, borderRadius: 10, width: 100, marginLeft: 20 }}>
                                    <Text style={{fontSize: 18, color: 'white', textAlign: 'center'}}>14:00</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </View>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Payment')}
                            style={{
                                width: "60%",
                                height: 50,
                                backgroundColor: '#95ca3e',
                                borderRadius: 50,
                                justifyContent: "center",
                                alignItems: "center",
                                marginVertical: 10,
                            }}
                        >
                            <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                    </ScrollView>
                </View>

                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    body: {
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 70
    },

});
