import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements'






export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'DETALLES DE LA ORDEN', style: { color: '#1c3643' } }}
                />



                <View style={styles.body}>
                    <ScrollView>
                        <View>
                            <Text>Detalles de la orden</Text> 
                        </View>
                        <View style={{justifyContent:"center", alignItems: "center"}}>
                            <AppButton color='white' backgroundColor='#3b5998' text='ACEPTAR' />
                            <AppButton color='white' backgroundColor='#db4437' text='CANCELAR' />
                            <AppButton color='white' backgroundColor='#1da1f2' text='ENTREGAR' />
                        </View>

                    </ScrollView>


                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    body: {
        width: "100%",
        padding: 20,
    },
    
});
