import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements';



export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                    }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null), }}
                    centerComponent={{ text: 'OPINIONES', style: { color: '#1c3643' } }}
                />



                <View style={styles.input}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{ width: 280, height: 'auto', margin: 10, backgroundColor: "#F3F3F3", color: "464646", padding: 10, borderRadius: 10, }}>
                            <View style={{ width: '100%', padding: 5, flexDirection: 'row', }}>
                                <View>
                                    <Text style={{ fontSize: 20, marginLeft: 5, fontWeight: '300', color: '#95ca3e', }}>
                                        Leudy Martes
                                            </Text>
                                    <Text style={{ fontSize: 12, marginLeft: 5, fontWeight: '300', color: '#333', }}>
                                        Peluquero profesional
                                            </Text>
                                </View>

                                <Text style={{ fontSize: 12, fontWeight: '400', color: '#BDBDBD', marginLeft: 'auto', marginTop: 15, alignItems: 'flex-end' }}>
                                    <Icon name="star" size={14} color="#FFCE4E" /> 4.6 Exelente (24)
                                        </Text>
                            </View>

                            <Text style={{ fontSize: 14, fontWeight: '400', color: '#333', textAlign: "justify", marginTop: 15, padding: 15 }}>
                                Lorem Ipsum es simplemente un texto ficticio de la industria de impresión y
                                composición tipográfica. Lorem Ipsum ha sido el texto ficticio estándar de la
                                industria desde el año 1500, cuando una impresora desconocida tomó una galera
                </Text>
                        </View>

                    </ScrollView>


                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        alignItems: 'center',
        textAlign: 'center',
        marginBottom: 10,
        width: '100%',
        flexDirection: 'row'
    }
});
