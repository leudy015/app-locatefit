import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity  } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements'




export default class Registration extends Component {

    render() {
        const { navigation } = this.props;
        
        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'AÑADIR CUPÓN', style: { color: '#1c3643' } }}
                />

                

                    <View style={styles.body}>
                    <View style={styles.input}> 
                        <InputField  placeholder="Añadir cupón" />
                    </View>

                    <AppButton color='white' backgroundColor='#95ca3e' text='Añadir cupón' onPress={() => alert('Cupón añadido con exito')} />

                   </View>    

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    body: {
        width: "100%",
        alignItems: 'center',
        marginTop:50

    },
    
});
