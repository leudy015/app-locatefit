import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem } from 'react-native-elements'
import  {Header}  from 'react-native-elements';
import Headerparallax from "./../../components/headerparallax";

const list = [
    {
        name: 'Servicio de Fontanería',
        avatar_url: 'https://www.canariasnoticias.es/sites/default/files/2019/01/61587.jpg',
        subtitle: 'Burgos'
    },
    {
        name: 'Servicio de Limpieza',
        avatar_url: 'http://www.residenciasarria.com/blog/wp-content/uploads/2016/10/productos-limpieza.jpg',
        subtitle: 'Madrid'
    },
]

{
    /*
    <Headerparallax text='Pedidos' descripcion='Aquí podrás gestionar tus pedidos, ver su estado y todo sus detalles' back={
                     <Text style={{color: 'white'}}onPress={() => navigation.goBack(null)} >Atrás</Text>
                }
                component={
                        <View>
                            {
                                list.map((l, i) => (
                                    <ListItem
                                        key={i}
                                        onPress={() => navigation.navigate('PedidoDitails')}
                                        leftAvatar={{ source: { uri: l.avatar_url } }}
                                        title={l.name}
                                        subtitle={l.subtitle}
                                        bottomDivider
                                        chevron
                                        
                                    />
                                ))
                            }
                        </View>
                }
                
                />
    
    */
}


export default class Registration extends Component {

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
            <Header
               containerStyle={{
                   backgroundColor: '#fff',
                 }}
               leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
               centerComponent={{ text: 'PEDIDOS', style: { color: '#1c3643' } }}
           />



           <View style={styles.body}>
               <ScrollView>
                   <View>
                       {
                           list.map((l, i) => (
                               <ListItem
                                   key={i}
                                   onPress={() => navigation.navigate('PedidoDitails')}
                                   leftAvatar={{ source: { uri: l.avatar_url } }}
                                   title={l.name}
                                   subtitle={l.subtitle}
                                   bottomDivider
                                   chevron
                                   
                               />
                           ))
                       }
                   </View>

               </ScrollView>


           </View>

       </View>

        );
    }
}

const styles = StyleSheet.create({
    
});
