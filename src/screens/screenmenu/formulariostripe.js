import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity  } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import  {Header}  from 'react-native-elements';




export default class Registration extends Component {

    render() {
        const { navigation } = this.props;
        
        return (
            <View style={styles.container}>
               <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'METÓDO DE PAGO', style: { color: '#1c3643' } }}
                />

                    <View style={styles.body}>
                        <Text>Aqui va el formulario de stripe</Text>

                   </View>    

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    body: {
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 60
    },

});
