import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard } from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import  {Header}  from 'react-native-elements';
import { Button, WhiteSpace, WingBlank } from '@ant-design/react-native';


export default class Registration extends Component {


    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'DIRECCIÓN', style: { color: '#1c3643' } }}
                />

                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={styles.body}>

                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Text style={{ color: '#202020', fontSize: 24, fontWeight: '400', marginBottom: 10, marginLeft: 10 }}>Añadir dirección</Text>

                        </View>

                        <View style={styles.body1}>
                        <View style={styles.input}>
                            <InputField placeholder="Nombre completo" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Calle" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Ciudad" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Provincia" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Código postal" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Número movil" />
                        </View>
                        </View>

                        <View style={styles.body1}>
                            <Button type="primary">Guardar Cambios</Button>
                        </View>
                    </View>
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    header: {

    },
    body1: {
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },

    body: {
        width: "100%",
        marginTop: 60,
        marginBottom: 100
    },

    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "90%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
       

    },
    input: {
        marginBottom: 20,
        backgroundColor: '#ddd',
        borderRadius: 8,
        width: '95%',
        flexDirection: 'row'
    }
});
