import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity  } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements'




export default class Registration extends Component {

    render() {
        const { navigation } = this.props;
        
        return (
            <View style={styles.container}>
               <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'COMPRA FINALIZADA CON ÉXITO', style: { color: '#1c3643' } }}
                />

                    <View style={styles.body}>
                    <View style={{alignItems: 'center', textAlign: 'center'}}> 
                    <Text>
                        <Icon name="check-circle" size={100} color="#95ca3e" /> 
                    </Text>

                    <Text style={{ color: '#555', fontSize: 18, fontWeight: '500',marginBottom: 30, textAlign: "center"}}>
                    ¡Profesional contratado con éxito en unos minutos te estará tocando el telefonillo!
                    </Text>

                    <Text style={{ color: '#999', fontSize: 14, fontWeight: '500',marginBottom: 30, textAlign: "center"}}>
                    Orden número: 27635 en unos minutos nuestro prefesional estara aceptando la orden.
                    </Text>
                     
                    </View>

                    <TouchableOpacity
                    onPress={() => navigation.navigate('Pedido')}
                    style={{
                        width: "60%",
                        height: 40,
                        backgroundColor: '#95ca3e',
                        borderRadius: 5,
                        bottom: 25,
                        justifyContent: "center",
                        alignItems: "center",
                        marginVertical: 10,
                    }}
                >
                    <Text style={{ fontSize: 16, alignItems: "center", color: '#FFFFFF', justifyContent: "center", }}>Ir a mis pedidos</Text>
                </TouchableOpacity>

                   </View>    

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    body: {
        width: "100%",
        alignItems: 'center',
        marginTop:50

    },
    
});
