import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity } from 'react-native';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Card, Header, Button } from 'react-native-elements'




export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
              <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'MIS ANUNCIOS', style: { color: '#1c3643' } }}
                />



                <View style={styles.body}>
                    <ScrollView>
                        <Card
                            image={require('./../../common/assets/1a22517de81e4424541c49f7fa2c2493694faef0.png')}>
                            <Text style={{ marginBottom: 10, fontSize: 20 }}>
                                Servicio de fontanería.
                            </Text>

                            <Text style={{ marginBottom: 10, color: "#999" }}>
                                The idea with React Native Elements is more about component structure than actual design.
                            </Text>
                            
                            <Button
                                icon={<Icon size={20} name='trash' color='#ffffff' />}
                                buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor: "red", borderRadius: 10 }}
                                title='Eliminar' />

                        </Card>
                    </ScrollView>


                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    cardtarjeta: {
        color: "#DDD",
        width: 300,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: '85%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 10,
        width: '95%',
        flexDirection: 'row'
    }
});
