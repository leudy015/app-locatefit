import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity  } from 'react-native';
import InputField from '../../components/Input';
import { ScrollView } from 'react-native-gesture-handler';
import  {Header}  from 'react-native-elements';
import { Button, WhiteSpace, WingBlank } from '@ant-design/react-native';




export default class Registration extends Component {
 


    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                  <Header
                    containerStyle={{
                        backgroundColor: '#fff',
                      }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null),}}
                    centerComponent={{ text: 'MIS PAGOS', style: { color: '#1c3643' } }}
                />

                <ScrollView showsHorizontalScrollIndicator={false}>

                    <View style={styles.body}>
                        <View>
                            <Text style={{ color: '#202020', width: "100%", fontSize: 24, fontWeight: '400', marginBottom: 10, textAlign: 'left', marginLeft: 10 }}>Añadir método de pago</Text>

                        </View>
                        <Text style={{ fontWeight: '200', width: 320, marginBottom: 30, textAlign: 'left', marginLeft: 10 }}>
                            Añade tu IBAM bancarío para retiro de servicios prestado.
                        </Text>

                        <View style={styles.body1}>
                        <View style={styles.input}>
                            <InputField placeholder="Nombre titular" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="IBAN" />
                        </View>                      
                    </View>
                    <View style={styles.body1}>
                            <Button type="primary">Guardar Cambios</Button>
                        </View>
                    </View>
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    body: {
        width: "100%",
        height: 'auto',
        marginTop: 60,
        marginBottom: 50
    },

    body1: {
        width: "100%",
        height: 'auto',
        alignItems: 'center',
        marginBottom: 20
    },
    
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        marginBottom: 20,
        backgroundColor: '#ddd',
        borderRadius: 8,
        width: '95%',
        flexDirection: 'row'
    },

    cardtarjeta: {
        color: "#DDD",
        width: 280,
        height: 60,
        backgroundColor: "#f8f9fa",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,

    },

});
