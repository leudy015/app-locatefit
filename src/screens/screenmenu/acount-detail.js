import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert, Keyboard } from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import InputField from '../../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { Header, Avatar } from 'react-native-elements';
import { Button, WhiteSpace, WingBlank } from '@ant-design/react-native';



export default class Registration extends Component {



    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <Header
                    containerStyle={{ backgroundColor: '#fff', }}
                    leftComponent={{ icon: 'chevron-left', color: '#95ca3e', onPress: () => navigation.goBack(null), }}
                    centerComponent={{ text: 'DETALLES DE LA CUENTA', style: { color: '#1c3643' } }}
                />

                <ScrollView>

                    <View style={styles.body}>

                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Text style={{ color: '#202020', fontSize: 24, fontWeight: '400', marginBottom: 10 }}>Edita tus  datos</Text>

                        </View>
                        <View style={{flexDirection: 'row'}}>
                        <Avatar
                            size="large"
                            rounded
                            source={{
                                uri:
                                    'https://server.locatefit.es/assets/images/1575286052768-file.png',
                            }}
                        />
                        <View style={{ marginLeft: 'auto', marginRight: 10, marginTop: 20}}>
                            <Button onPress={() => navigation.navigate('SocialLogin')} size='small' type="warning">Cerrar sesión</Button>
                        </View>
                        </View>
                        <View style={styles.body1}>
                        <View style={styles.input}>
                            <InputField placeholder="Nombre" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Apellidos" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Profesión" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Correo eléctronico" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Teléfono" />
                        </View>

                        <View style={styles.input}>
                            <InputField placeholder="Descripción" />
                        </View>
                        </View>
                        <View style={styles.body1}>
                            <Button type="primary">Guardar Cambios</Button>
                        </View>

                        
                    </View>
                </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    icons: {
        marginTop: 20

    },

    header: {
        width: "100%",
        height: '15%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body1: {
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    body: {
        width: "100%",
        marginTop: 60,
        marginBottom: 50
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textView: {
        width: "100%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",

    },
    input: {
        marginBottom: 20,
        backgroundColor: '#ddd',
        borderRadius: 8,
        width: '95%',
        flexDirection: 'row'
    }
});
