import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';
import Astronauta from './astronauta.png';
import Trazo from './trazo.png';

export default class Recoverypassword extends Component {
   
    render() {

        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{ color: '#ffffff', fontSize: 14, fontWeight: '400', marginTop: 40, marginRight: 'auto', marginBottom: 30, marginLeft: 10 }} onPress={() => navigation.goBack(null)}><Icon name="chevron-left" size={22} color="#95ca3e" /></Text>
                    {/*<Image
                            style={{
                                width: 180, 
                                height: 33
                                }}
                            source={Logo}
                        />
                        <Image
                            style={styles.header1}
                            source={Header}
                            />*/}
                </View>
                    <View style={styles.body}>
                        <View
                            style={[styles.textView], {
                                marginBottom: 15,
                            }}>
                            <Animatable.Text style={{ color: '#95ca3e', fontSize: 24, fontWeight: '400', textAlign: "center", marginTop: 40, padding: 20 }} animation="bounceInDown" duration={3000}>Consigue 20% de descuento con este cupón</Animatable.Text>

                            <Animatable.Text animation="bounceInDown" duration={2000} style={{ fontWeight: '200', color: "#aaa", textAlign: "center", marginBottom: 0, padding: 10}}>
                            Cupón válido sólo para compras mayores de 50€, ciertas restricciones aplican.
                        </Animatable.Text>
                        </View>
                        

                        <Image
                            style={styles.trazo}
                            source={Trazo}
                            />
                            <View style={{marginTop: -160, backgroundColor: 'white', padding: 10, width: '60%', borderRadius: 50 }}><Text style={{textAlign: 'center', fontSize: 30, fontFamily: 'Helvetica', fontWeight: 'bold', color: '#1C3643'  }}>ALLSTARTUP</Text></View>
                    </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    astronauta: {
        marginTop: 'auto',
        width: 200,
        height: 200
    },

    trazo:{
        width: '100%',
        height: 300,
    },

    icons: {
        marginTop: 17

    },

    header: {
        width: "100%",
        height: 'auto',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',

    },
    body: {
        width: "100%",
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Logo: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    textView: {
        width: "85%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
        marginBottom: 30,
        width: '90%',
        flexDirection: 'row'
    }
});