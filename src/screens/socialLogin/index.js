import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground} from 'react-native';
import Logo from '../../common/assets/logo.png';
import AppButton from '../../components/Button';
import Header from "../../common/header.png";
import Headers from "../../common/headers.png";
import { SocialIcon } from 'react-native-elements'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';

export default class SocialLogin extends Component {
  
    render() {

        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <ImageBackground source={Headers} style={{width: '100%', height: 200}}/>    
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.body}>
                    <View
                        style={[styles.textView], {
                        }}>
                            <Animatable.Text style={{ color: '#1c3643', fontSize: 24, fontWeight: '400' }} animation="bounceInDown" duration={3000}>Bienvenido a Locatefit</Animatable.Text>
                    </View>
                    <View
                        style={[styles.textView], {
                            marginTop: 5
                        }}>
                        <Animatable.Text style={{ fontSize: 16, fontWeight: '200', color: "#A19D9D", marginBottom: 20 }} animation="bounceInDown" duration={2000}>Continua con</Animatable.Text>
                    </View>
                    <View style={{ width:"80%" }}>

                    
                    <SocialIcon
                        onPress={() => alert('Pronto podras iniciar con Facebook')}
                        title='Continua con Facebook'
                        button
                        type='facebook'
                    />
                    

                    <SocialIcon
                        onPress={() => alert('Pronto podras iniciar con Twitter')}
                        title='Continua con Twitter'
                        button
                        type='twitter'
                    />

                    <SocialIcon
                        onPress={() => alert('Pronto podras iniciar con Google')}
                        title='Continua con Google'
                        button
                        type='google'
                    />
                    </View>

                    <View
                        style={[styles.textView], {
                            height: 30,
                            marginVertical: 5,
                        }}>
                        <Text style={{ fontSize: 16, fontWeight: '200', color: "#A19D9D" }}>O</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('List')}
                        style={{
                            width: "80%",
                            height: 40,
                            backgroundColor: "#1c3643",
                            borderRadius: 6,
                            justifyContent: "center",
                            alignItems: "center",
                            marginVertical: 10,
                        }}
                    >
                        <Text style={{ fontSize: 18, color: "white" }}>Explora Locatefit</Text>
                    </TouchableOpacity>
                    <View
                        style={[styles.textView], {
                            marginVertical: 5,
                        }}>
                        <Text style={{ color: '#1c3643', fontSize: 16, fontWeight: 'bold', marginTop: 50, textAlign: 'center', marginBottom: 10 }} onPress={() => navigation.navigate('Registration')}>Registrate</Text>
                        <Text style={{ fontWeight: '200', color: "#A19D9D" }}>
                            ¿Tienes una cuenta?
                            <Text style={{ color: '#1c3643', fontSize: 16, fontWeight: 'bold', }} onPress={() => navigation.navigate('Login')} > Inicia sesión</Text>
                        </Text>
                    </View>
                    <View
                        style={[styles.textView], {
                            marginBottom: 5,
                            marginTop: 12
                        }}>
                        <Text style={{ fontWeight: '200', color: '#d7d7d7', textAlign: 'center', padding: 30 }}>
                            Continuar implica que has leído y aceptado los
                            <Text style={{ color: '#38aef6' }}> Términos y condiciones</Text>
                        </Text>
                    </View>
                </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    header1: {
        marginTop: 'auto',
        width: '100%',
        height: 80
    },

    header: {
        width: "100%",
        height: 230,
        backgroundColor: '#1c3643',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 80
    },
    body: {
        width: "100%",
        height: 'auto',
        marginTop: '25%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    textView: {
        width: "85%",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 100
    }
});
