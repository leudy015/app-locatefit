import * as React from "react"
import { TextInput } from "react-native";

const InputField = props => {
    console.log('placeholder', props.placeholder)
    return (
        <TextInput
            style={{
                width: props.width ? props.width : "100%",
                height: 40,
                borderRadius: 4,
                paddingLeft: props.paddingLeft ? props.paddingLeft : 12,
                marginTop: 10,
                color: 'black'
            }}
            placeholderTextColor='grey'
            placeholder={props.placeholder}
        />
    )
}
export default InputField;