import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';

export default AppButton = props => {
    return (
        <TouchableOpacity
            style={{
                width: "70%",
                height: 40,
                backgroundColor: props.backgroundColor,
                borderRadius: 6,
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 10,
            }}
        >
            <Text  style={{ fontSize: 18, color: props.color }}>{props.text}</Text>
        </TouchableOpacity>
    );
}
