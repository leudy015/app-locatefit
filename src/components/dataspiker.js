import React, { Component } from "react";
import { Button, View, Text } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";

export default class DateTimePickerTester extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false
    };
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleDatePicked = time => {
    console.log("A date has been picked: ", time);
    this.hideDateTimePicker();
  };

  render() {
    return (
      <>
        <Text style={{marginTop: 10, marginLeft: 20, borderWidth: 0.5,
        borderColor: '#d6d7da', padding: 10, borderRadius: 5,  color: '#9EA0A4'}} onPress={this.showDateTimePicker} >Duración del servicio</Text>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          mode="time"
          locale="es_EU" // Use "en_GB" here
          date={new Date()}
        />
      </>

    );
  }
}