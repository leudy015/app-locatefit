import React, { Component } from 'react';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import { StyleSheet, Text, View, Image, Alert, Keyboard, TouchableOpacity, Dimensions, } from 'react-native';
import { Icon } from 'react-native-elements';
import Header from "./../common/header.png";
import ParallaxScrollView from 'react-native-parallax-scroll-view';


//<Image style={styles.header1} source={Header}/>
// Inside of a component's render() method:
export default Headersparallax = props => {

  return (
    
    <ParallaxScrollView
    showsVerticalScrollIndicator={false}
    headerBackgroundColor="white"
    stickyHeaderHeight={ STICKY_HEADER_HEIGHT }
    parallaxHeaderHeight={ PARALLAX_HEADER_HEIGHT }
    backgroundSpeed={10}

    renderBackground={() => (
      
      <View key="background">
        <View style={{position: 'absolute',
                      top: 0,
                      width: window.width,
                      backgroundColor: '#1c3643',
                      height: PARALLAX_HEADER_HEIGHT}}/>
      </View>
    )}

    renderForeground={() => (
      <View key="parallax-header" style={ styles.parallaxHeader }>
        <Text style={ styles.sectionSpeakerText }>
          {props.name}
        </Text>
        <Text style={ styles.sectionTitleText }>
          {props.descripcion}
        </Text>
      </View>
    )}
    renderFixedHeader={() => (
      <View key="fixed-header" style={styles.fixedSection}>
        <View style={styles.fixedSectionText}>
          {props.back1}
        </View>
        <View style={styles.fixedSectionText}>
          {props.back}
        </View>
      </View>
    )}


    renderStickyHeader={() => (
      <View key="sticky-header" style={styles.stickySection}>
    <Text style={styles.stickySectionText}>{props.name}</Text>
      </View>
    )}
    >
      <View style={{ height: 'auto', width: '100%', marginTop: 20 }}>
        {props.component}
      </View>
    </ParallaxScrollView>
  );
}



const window = Dimensions.get('window');

const AVATAR_SIZE = 90;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 150;
const STICKY_HEADER_HEIGHT = 70;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
},
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    width: '100%',
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: '100%',
    backgroundColor: '#1c3643',
    justifyContent: 'flex-end'
  },
  stickySectionText: {
    color: 'white',
    fontSize: 20,
    margin: 10,
  },
  fixedSection: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    flexDirection: 'row'   
  },
  fixedSectionText: {
    color: '#999',
    fontSize: 16,
    marginRight: 10
  },
  parallaxHeader: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    paddingTop: 70
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2,
    borderWidth: 2,
    borderColor: '#d6d7da',
    width: AVATAR_SIZE, 
    height: AVATAR_SIZE
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    padding: 10,
    textAlign: 'left'
  },
  sectionTitleText: {
    color: '#999',
    fontSize: 14,
    textAlign: 'left',
    padding: 10,
    marginTop: -20
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: 'white',
    borderColor: '#ccc',
    borderBottomWidth: 1,
  },
  rowText: {
    fontSize: 20
  },

  header1:{
    width: '100%',
    height: 80,
    marginTop: 'auto'
},


});

