/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import AppNavigator from './src/navigations';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { NETWORK_INTERFACE } from './src/config';

const apolloClient = new ApolloClient({
  link: new HttpLink({ uri: NETWORK_INTERFACE}),
  cache: new InMemoryCache()
})

export default class App extends Component {
  render() {
    return (
        <ApolloProvider client={apolloClient}>
            <AppNavigator />
        </ApolloProvider>
    );
  }
}
